import random

def generate_random_passwor(max_len):
    res = False
    if max_len >= 4:
        password = ""
        digits = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
        low_case_chr = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
                             'i', 'j', 'k', 'm', 'n', 'o', 'p', 'q',
                             'r', 's', 't', 'u', 'v', 'w', 'x', 'y',
                             'z']

        upr_case_chr = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                             'I', 'J', 'K', 'M', 'N', 'O', 'p', 'Q',
                             'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y',
                             'Z']

        symbols = ['@', '#', '$', '%', '=', ':', '?', '.', '/', '|', '~', '>',
                   '*', '(', ')', '<']

        Combind_list = digits + upr_case_chr + low_case_chr + symbols

        random_digit = random.choice(digits)
        random_symbol = random.choice(symbols)
        random_upr_case = random.choice(upr_case_chr)
        random_lwr_case = random.choice(low_case_chr)

        tem_pass = random_digit + random_symbol + random_upr_case + random_lwr_case

        for i in range(max_len-4):
            password += random.choice(Combind_list)

        password += tem_pass
        print(password)

        res = True
    else:
        print("Min length of password should be 4")
    return res

if __name__ == "__main__":
    print("password length should be between 4 to user max length")
    print("Enter MAX length of password : ", end='')
    max_len = int(input())
    Result = generate_random_passwor(max_len)
    print(Result)